import time
import os
import io
import math
import logging
import json,sys
import requests
import re
import boto3
import pickle
from datetime import datetime
from multiprocessing import Pool, Process, Pipe
import urllib3
import codecs
from settings import AWS_KEY, AWS_SECRET

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

kw = {}
if len(sys.argv) > 1:
    kw["name"] = sys.argv[1]
    kw["filepath"] = sys.argv[2]
    

client = boto3.client('s3',aws_access_key_id=AWS_KEY,aws_secret_access_key=AWS_SECRET)


if __name__ == "__main__": 
    try:
        URL="https://accounts.eu1.gigya.com/accounts.getScreenSets"
       
        headers={"Content-Type": "application/x-www-form-urlencoded"}
        
        body={"apiKey":"3_0Yr3W4fSAgJHHCnKgVrlj4nWXXNtSZnXRQxUndHgQ1_f3JjlYqXyp7DfGSsMx4nw",
        "userKey":"AEGdgEneDEXP",
        "secret":"AqD7I9I8cGJFbqoscejbZo0E+EBe4fIJ",
        "include":"html,css,javascript,translations,metadata",
        "screenSetIDs":"ULVR_newsletter_v2_CA"}
        
        
        res = requests.post(URL, data=body, headers=headers,verify = False)
        r_json=res.json()
        html=r_json["screenSets"][0]["html"]
        css=r_json["screenSets"][0]["css"]
        javascript=r_json["screenSets"][0]["javascript"]
        metadata=r_json["screenSets"][0]["metadata"]
        translations=r_json["screenSets"][0]["translations"]
        
        
        html=html.replace('\\\"', '"')
        html=html.replace('\n', '')
        html=html.replace('\t"', '')
        html=html.replace('ULVR_newsletter_v2_CA', kw["name"])
       
        
        css=css.replace('\\\"', '"')
        css=css.replace('\n', '')
        css=css.replace('\t"', '')
      
        
        javascript=javascript.replace('\\\"', '"')
        javascript=javascript.replace('\n', '')
        javascript=javascript.replace('\t"', '')
        javascript=javascript.replace('onBeforeValidation', '\n onBeforeValidation')
        

        with codecs.open('metadata.json', 'w', encoding='utf-8') as f:
            json.dump(metadata, f, ensure_ascii=False)  
        
        with codecs.open('translations.json', 'w', encoding='utf-8') as f:
            json.dump(translations, f, ensure_ascii=False)
            
        response = client.put_object(
        Body=html,
        Bucket='cpp-gigya-form-journey-templates',
        Key=kw["filepath"]+"/html.html",
        )
        
        response = client.put_object(
        Body=css,
        Bucket='cpp-gigya-form-journey-templates',
        Key=kw["filepath"]+"/css.css",
        )
        
        response = client.put_object(
        Body=javascript,
        Bucket='cpp-gigya-form-journey-templates',
        Key=kw["filepath"]+"/js.js",
        )

        
        response = client.upload_file(
        'metadata.json',
        'cpp-gigya-form-journey-templates',
        kw["filepath"]+"/metadata",
        )
        
        response = client.upload_file(
        'translations.json',
        'cpp-gigya-form-journey-templates',
        kw["filepath"]+"/translations.json",
        )
        
        if os.path.exists("metadata.json"):
            os.remove("metadata.json")
            print("metadata.json removed")
        else:
            print("metadata.json file does not exist")
        
        if os.path.exists("translations.json"):
            os.remove("translations.json")
            print("translations.json removed")
        else:
            print("translations.json file does not exist")        
        
            
    except Exception as err:
        print(err)